package com.danielm59.bcukbot.discord.pointSystem;

import com.danielm59.bcukbot.configs.JsonData;
import com.danielm59.bcukbot.discord.DiscordBot;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

import java.io.File;
import java.util.TreeMap;

public class PointsRewards extends TreeMap<Integer, Long> implements JsonData
{
	private static final transient File rewardData = new File("PointsRewards.json");

	void process(IUser user, int points)
	{
		if (this.size() > 0)
		{
			int rewardLevel = this.floorKey(points);
			if (this.containsKey(rewardLevel))
			{
				IRole reward = DiscordBot.getRole(this.get(rewardLevel));
				if (!user.hasRole(reward))
				{
					user.addRole(reward);
				}
			}
		}
	}

	@Override
	public File getFile()
	{
		return rewardData;
	}
}
