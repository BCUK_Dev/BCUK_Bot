package com.danielm59.bcukbot.discord.pointSystem;

import com.danielm59.bcukbot.configs.JsonData;
import sx.blah.discord.handle.obj.IUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class PointData extends HashMap<Long, PointData.Member> implements JsonData
{
	private final transient File dataFile = new File("PointData.json");

	@Override
	public File getFile()
	{
		return dataFile;
	}

	private ArrayList<Long> getOrder()
	{
		ArrayList<Long> order = new ArrayList<>(this.keySet());
		order.sort(Comparator.comparingInt(o -> this.get(o).points).reversed());
		return order;
	}

	public int getRank(IUser user)
	{
		ArrayList<Long> order = getOrder();
		return order.indexOf(user.getLongID()) + 1;
	}

	class Member
	{
		String name = "";
		int points = 0;
		long lastXPTime = 0;
	}
}
