package com.danielm59.bcukbot.discord.pointSystem;

import com.danielm59.bcukbot.Main;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class PointSystem
{
	private static final int pointsMin = 15;
	private static final int pointsMax = 25;

	private static final Random RNG = new Random();

	private static PointData pointData = new PointData();
	private static PointsRewards pointsRewards = new PointsRewards();

	public static void init()
	{
		pointData = Main.loadJson(pointData);
		pointsRewards = Main.loadJson(pointsRewards);
	}

	public static void addXP(IUser user)
	{
		Long time = System.currentTimeMillis();
		//Check if member exists
		Long userID = user.getLongID();
		if (!pointData.containsKey(userID))
		{
			pointData.put(userID, pointData.new Member());
		}
		PointData.Member member = pointData.get(userID);
		member.name = user.getName(); //Keep name up to date as much as possible to make manual edits easy
		//Anti spam check
		if (time - member.lastXPTime > TimeUnit.MINUTES.toMillis(1))
		{
			member.lastXPTime = time;
			int points = RNG.nextInt(pointsMax - pointsMin + 1) + pointsMin;
			member.points += points;
			pointsRewards.process(user, member.points);
			Main.saveJson(pointData);
		}
	}

	public static void points(IChannel channel, IUser user)
	{
		EmbedBuilder builder = new EmbedBuilder();
		PointData.Member member = pointData.get(user.getLongID());
		builder.withAuthorIcon(user.getAvatarURL());
		builder.withAuthorName(channel.isPrivate() ? user.getName() : user.getDisplayName(channel.getGuild()));
		builder.appendField("Rank", String.format("%d/%d", pointData.getRank(user), pointData.size()), true);
		builder.appendField("points", String.format("%d", member.points), true);
		RequestBuffer.request(() -> channel.sendMessage(builder.build()));
	}

}
