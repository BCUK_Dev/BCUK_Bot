package com.danielm59.bcukbot.discord.music;

import com.danielm59.bcukbot.Main;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;

/**
 * Holder for both the player and a track scheduler for one guild.
 */
class GuildMusicManager
{
	/**
	 * Track scheduler for the player.
	 */
	final TrackScheduler scheduler;
	/**
	 * Audio player for the guild.
	 */
	private final AudioPlayer player;

	/**
	 * Creates a player and a track scheduler.
	 *
	 * @param manager Audio player manager to use for creating the player.
	 */
	GuildMusicManager(AudioPlayerManager manager)
	{
		player = manager.createPlayer();
		scheduler = new TrackScheduler(player);
		player.addListener(scheduler);
		setVolume(Main.musicConfig.getVolume());
		scheduler.setLoop(false);
	}

	/**
	 * @return Wrapper around AudioPlayer to use it as an AudioSendHandler.
	 */
	AudioProvider getAudioProvider()
	{
		return new AudioProvider(player);
	}

	int getVolume()
	{
		return player.getVolume();
	}

	void setVolume(int level)
	{
		player.setVolume(level);
	}


	public Boolean togglePause()
	{
		player.setPaused(!player.isPaused());
		return player.isPaused();
	}

	public void resume()
	{
		player.setPaused(true);
		player.setPaused(false);
	}

}
