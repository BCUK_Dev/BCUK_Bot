package com.danielm59.bcukbot.discord.music;

import com.danielm59.bcukbot.Main;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import org.apache.commons.lang3.ArrayUtils;
import sx.blah.discord.handle.audio.IAudioManager;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MusicHandler
{
	private static final AudioPlayerManager playerManager = new DefaultAudioPlayerManager();

	private static final Map<Long, GuildMusicManager> musicManagers = new HashMap<>();


	static
	{
		AudioSourceManagers.registerRemoteSources(playerManager);
		AudioSourceManagers.registerLocalSource(playerManager);
	}

	private static void connectToVoiceChannel(IAudioManager audioManager)
	{
		for (IVoiceChannel voiceChannel : audioManager.getGuild().getVoiceChannels())
		{
			if (voiceChannel.isConnected())
			{
				return;
			}
		}
		for (IVoiceChannel voiceChannel : audioManager.getGuild().getVoiceChannels())
		{
			try
			{
				if (voiceChannel.getName().equals(Main.musicConfig.getRoom()))
				{
					RequestBuffer.request(voiceChannel::join).get();
					GuildMusicManager musicManager = getGuildAudioPlayer(audioManager.getGuild());
					musicManager.setVolume(Main.musicConfig.getVolume());
					break;
				}

			} catch (MissingPermissionsException e)
			{
				Main.getLogger().warn("Cannot enter voice channel {}", voiceChannel.getName(), e);
			}
		}
	}

	private static synchronized GuildMusicManager getGuildAudioPlayer(IGuild guild)
	{
		long guildId = guild.getLongID();
		GuildMusicManager musicManager = musicManagers.computeIfAbsent(guildId, k -> new GuildMusicManager(playerManager));

		guild.getAudioManager().setAudioProvider(musicManager.getAudioProvider());

		return musicManager;
	}

	public static void loadAndPlay(final IChannel channel, final String trackUrl)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

		playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler()
		{
			@Override
			public void trackLoaded(AudioTrack track)
			{
				RequestBuffer.request(() -> channel.sendMessage("Adding to queue: " + track.getInfo().title));
				play(channel.getGuild(), musicManager, track, musicManager.scheduler.loop);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist)
			{
				List<AudioTrack> tracks = playlist.getTracks();
				Collections.shuffle(tracks);
				for (AudioTrack track : tracks)
				{
					play(channel.getGuild(), musicManager, track, true);
				}
				RequestBuffer.request(() -> channel.sendMessage("Adding playlist to queue: " + playlist.getName()));
			}

			@Override
			public void noMatches()
			{
				RequestBuffer.request(() -> channel.sendMessage("Nothing found by " + trackUrl));
			}

			@Override
			public void loadFailed(FriendlyException exception)
			{
				RequestBuffer.request(() -> channel.sendMessage("Could not play: " + exception.getMessage()));
			}
		});
	}

	private static void play(IGuild guild, GuildMusicManager musicManager, AudioTrack track, boolean loop)
	{
		connectToVoiceChannel(guild.getAudioManager());
		musicManager.scheduler.setLoop(loop);
		musicManager.scheduler.queue(track);
	}

	public static void skipTrack(IChannel channel)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
		musicManager.scheduler.nextTrack();

		RequestBuffer.request(() -> channel.sendMessage("Skipped to next track."));
	}

	public static void stopAndClear(IChannel channel, boolean leave)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
		musicManager.scheduler.setLoop(false);
		musicManager.scheduler.clear();
		musicManager.scheduler.nextTrack();
		RequestBuffer.request(() -> channel.sendMessage("Music stopped and queue cleared"));
		if(leave)
		{
			for (IVoiceChannel voiceChannel : channel.getGuild().getVoiceChannels())
			{
				if (voiceChannel.isConnected())
				{
					voiceChannel.leave();
					RequestBuffer.request(() -> channel.sendMessage("Goodbye"));
					break;
				}
			}
		}
	}

	public static void listTracks(IChannel channel, int page)
	{
		GuildMusicManager manager = getGuildAudioPlayer(channel.getGuild());
		int length = manager.scheduler.getPlaylist().size();
		int pages = length / 10 + (length % 10 > 0 ? 1 : 0);
		page = page > pages ? pages : page;
		if (length > 0)
		{
			StringBuilder s = new StringBuilder();
			AudioTrack[] tracks = new AudioTrack[]{};
			tracks = ArrayUtils.subarray(manager.scheduler.getPlaylist().toArray(tracks), (page - 1) * 10, page * 10);
			int i = 1;
			s.append(String.format("Page %d of %d\n", page, pages));
			for (AudioTrack track : tracks)
			{
				s.append(String.format("[%d] ", (page - 1) * 10 + i++));
				s.append(track.getInfo().title);
				s.append("\n");
			}
			RequestBuffer.request(() -> channel.sendMessage(s.toString()));
		} else
		{
			RequestBuffer.request(() -> channel.sendMessage("No Songs in playlist"));
		}

	}

	public static void playing(IChannel channel)
	{
		GuildMusicManager manager = getGuildAudioPlayer(channel.getGuild());
		AudioTrack track = manager.scheduler.currentTrack;
		if (track != null)
		{
			RequestBuffer.request(() -> channel.sendMessage("Playing: " + track.getInfo().title));
		} else
		{
			RequestBuffer.request(() -> channel.sendMessage("No Track is currently Playing"));
		}
	}

	public static void playPlaylist(IChannel channel, String playlist)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
		musicManager.scheduler.clear();
		musicManager.scheduler.nextTrack();

		RequestBuffer.request(() -> channel.sendMessage("Changing Playlist"));

		loadAndPlay(channel, playlist);
	}

	public static void setVolume(IChannel channel, String volume)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
		musicManager.setVolume(Integer.parseInt(volume));
		RequestBuffer.request(() -> channel.sendMessage("Volume set to " + musicManager.getVolume()));
		Main.musicConfig.setVolume(Integer.parseInt(volume));

	}

	public static void getVolume(IChannel channel)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
		RequestBuffer.request(() -> channel.sendMessage("Volume set to " + musicManager.getVolume()));
	}

	public static void togglePause(IChannel channel)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
		Boolean isPaused = musicManager.togglePause();
		if (isPaused)
		{
			RequestBuffer.request(() -> channel.sendMessage("music Paused"));
		} else
		{
			RequestBuffer.request(() -> channel.sendMessage("music UnPaused"));
		}
	}

	public static void resume(IGuild guild)
	{
		GuildMusicManager musicManager = getGuildAudioPlayer(guild);
		musicManager.resume();
	}
}
