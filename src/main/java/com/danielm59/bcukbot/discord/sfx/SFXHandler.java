package com.danielm59.bcukbot.discord.sfx;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.discord.DiscordBot;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.IVoiceState;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

import java.util.HashMap;
import java.util.Map;

public class SFXHandler
{
	private static final AudioPlayerManager playerManager = new DefaultAudioPlayerManager();

	private static final Map<Long, GuildSFXManager> musicManagers = new HashMap<>();


	static
	{
		AudioSourceManagers.registerRemoteSources(playerManager);
		AudioSourceManagers.registerLocalSource(playerManager);
	}

	private static void connectToVoiceChannel(IVoiceChannel voiceChannel)
	{
		if (voiceChannel.isConnected())
		{
			return;
		}
		try
		{
			RequestBuffer.request(voiceChannel::join).get();
			GuildSFXManager musicManager = getGuildAudioPlayer(voiceChannel.getGuild());
			musicManager.setVolume(Main.sfxFiles.getVolume());

		} catch (MissingPermissionsException e)
		{
			Main.getLogger().warn("Cannot enter voice channel {}", voiceChannel.getName(), e);
		}
	}

	private static synchronized GuildSFXManager getGuildAudioPlayer(IGuild guild)
	{
		long guildId = guild.getLongID();
		GuildSFXManager musicManager = musicManagers.computeIfAbsent(guildId, k -> new GuildSFXManager(guild, playerManager));

		guild.getAudioManager().setAudioProvider(musicManager.getAudioProvider());

		return musicManager;
	}

	public static void loadAndPlay(final Long discordID, final String trackUrl)
	{
		IVoiceChannel voiceChannel = findUser(discordID);
		if (voiceChannel == null)
		{
			//ERROR
			return;
		}
		GuildSFXManager musicManager = getGuildAudioPlayer(voiceChannel.getGuild());

		playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler()
		{
			@Override
			public void trackLoaded(AudioTrack track)
			{
				//RequestBuffer.request(() -> channel.sendMessage("Adding to queue: " + track.getInfo().title));
				play(voiceChannel, musicManager, track);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist)
			{
				//SFX will not be a playlist
			}

			@Override
			public void noMatches()
			{

				Main.getLogger().error("SFX not found by " + trackUrl);
			}

			@Override
			public void loadFailed(FriendlyException exception)
			{
				Main.getLogger().error("Could not play SFX: " + exception.getMessage());
			}
		});
	}

	private static void play(IVoiceChannel voiceChannel, GuildSFXManager musicManager, AudioTrack track)
	{
		connectToVoiceChannel(voiceChannel);
		musicManager.scheduler.queue(track);
	}

	private static IVoiceChannel findUser(Long discordID)
	{
		IUser user = DiscordBot.getUser(discordID);
		for (IVoiceState voiceStatus : user.getVoiceStates().values())
		{
			if (voiceStatus.getChannel() != null)
			{
				return voiceStatus.getChannel();
			}
		}
		return null;
	}
}
