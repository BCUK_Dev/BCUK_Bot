package com.danielm59.bcukbot.discord.sfx;

import com.danielm59.bcukbot.discord.music.MusicHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import sx.blah.discord.handle.obj.IGuild;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This class schedules tracks for the audio player. It contains the queue of tracks.
 */
public class SFXScheduler extends AudioEventAdapter
{
	private final IGuild guild;
	private final AudioPlayer player;
	private final BlockingQueue<AudioTrack> queue;


	/**
	 * @param player The audio player this scheduler uses
	 */
	SFXScheduler(IGuild guild, AudioPlayer player)
	{
		this.guild = guild;
		this.player = player;
		this.queue = new LinkedBlockingQueue<>();
	}

	/**
	 * Add the next track to queue or play right away if nothing is in the queue.
	 *
	 * @param track The track to play or add to queue.
	 */
	void queue(AudioTrack track)
	{
		// Calling startTrack with the noInterrupt set to true will start the track only if nothing is currently playing. If
		// something is playing, it returns false and does nothing. In that case the player was already playing so this
		// track goes to the queue instead.
		if (!player.startTrack(track, true))
		{
			queue.offer(track);
		}
	}

	/**
	 * Start the next track, stopping the current one if it is playing.
	 */
	private void nextTrack()
	{
		// Start the next track, regardless of if something is already playing or not. In case queue was empty, we are
		// giving null to startTrack, which is a valid argument and will simply stop the player.
		if (queue.isEmpty())
		{
			MusicHandler.resume(guild);
		} else
		{
			player.startTrack(queue.poll(), false);
		}
	}

	@Override
	public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason)
	{
		// Only start the next track if the end reason is suitable for it (FINISHED or LOAD_FAILED)
		if (endReason.mayStartNext)
		{
			nextTrack();
		}
	}
}