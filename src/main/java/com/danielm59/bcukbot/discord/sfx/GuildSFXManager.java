package com.danielm59.bcukbot.discord.sfx;

import com.danielm59.bcukbot.Main;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import sx.blah.discord.handle.obj.IGuild;

/**
 * Holder for both the player and a track scheduler for one guild.
 */
class GuildSFXManager
{
	/**
	 * Track scheduler for the player.
	 */
	final SFXScheduler scheduler;
	/**
	 * Audio player for the guild.
	 */
	private final AudioPlayer player;

	/**
	 * Creates a player and a track scheduler.
	 *
	 * @param manager Audio player manager to use for creating the player.
	 */
	GuildSFXManager(IGuild guild, AudioPlayerManager manager)
	{
		player = manager.createPlayer();
		scheduler = new SFXScheduler(guild, player);
		player.addListener(scheduler);
		setVolume(Main.musicConfig.getVolume());
	}

	/**
	 * @return Wrapper around AudioPlayer to use it as an AudioSendHandler.
	 */
	SFXAudioProvider getAudioProvider()
	{
		return new SFXAudioProvider(player);
	}

	int getVolume()
	{
		return player.getVolume();
	}

	void setVolume(int level)
	{
		player.setVolume(level);
	}
}
