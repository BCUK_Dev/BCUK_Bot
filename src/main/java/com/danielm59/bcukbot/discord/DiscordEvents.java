package com.danielm59.bcukbot.discord;

import com.danielm59.bcukbot.commands.discord.DiscordCommands;
import com.danielm59.bcukbot.discord.pointSystem.PointSystem;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.PermissionUtils;
import sx.blah.discord.util.RequestBuffer;

public class DiscordEvents
{

	public static DiscordCommands commands = new DiscordCommands();

	@EventSubscriber
	public void onMessageReceived(MessageReceivedEvent event)
	{
		IMessage message = event.getMessage();

		String[] command = message.getContent().split(" ", 2);

		if (!message.getAuthor().isBot() && message.getGuild() != null) PointSystem.addXP(message.getAuthor());

		if (commands.containsKey(command[0].toLowerCase()))
		{
			String[] args = {};
			if (command.length > 1)
				args = command[1].split(" ");
			DiscordCommands.Command com = commands.get(command[0].toLowerCase());
			if (PermissionUtils.hasPermissions(message.getChannel(), message.getAuthor(), com.getPermission()))
			{
				com.getFunction().accept(message.getChannel(), message.getAuthor(), args);
			} else
			{
				RequestBuffer.request(() -> message.getChannel().sendMessage("You can't do that!"));
			}
		}
	}
}