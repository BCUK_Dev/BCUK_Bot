package com.danielm59.bcukbot.discord;

import sx.blah.discord.handle.impl.obj.ReactionEmoji;

public class Emoji
{
	public static final ReactionEmoji A = ReactionEmoji.of("\uD83C\uDDE6");
	public static final ReactionEmoji B = ReactionEmoji.of("\uD83C\uDDE7");
	public static final ReactionEmoji C = ReactionEmoji.of("\uD83C\uDDE8");
	public static final ReactionEmoji D = ReactionEmoji.of("\uD83C\uDDE9");

	public static final ReactionEmoji TRUE = ReactionEmoji.of("\u2705");
	public static final ReactionEmoji FALSE = ReactionEmoji.of("\uD83D\uDEAB");
}
