package com.danielm59.bcukbot.sql;

import com.danielm59.bcukbot.Main;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLServer
{
	public static String add(String type, String data, String source)
	{
		try
		{
			String server = String.format("jdbc:mysql://%s/", Main.config.getSQLServer());
			Connection con = DriverManager.getConnection(server, Main.config.getSQLUser(), Main.config.getSQLPassword());
			PreparedStatement preparedStmt = con.prepareStatement(String.format("insert into Quotes.%ss (%s, Source, Date) values (?, ?, ?)", type, type));
			preparedStmt.setString(1, data);
			preparedStmt.setString(2, source);
			preparedStmt.setString(3, new Date(System.currentTimeMillis()).toString());
			preparedStmt.execute();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("select * from Quotes.%ss order by ID Desc limit 1", type));
			rs.next();
			String quote = String.format("Added %s %d: %s [%s]", type, rs.getInt(1), rs.getString(2), rs.getDate(4).toString());
			con.close();
			return quote;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static String get(String type, int i)
	{
		try
		{
			String server = String.format("jdbc:mysql://%s/", Main.config.getSQLServer());
			Connection con = DriverManager.getConnection(server, Main.config.getSQLUser(), Main.config.getSQLPassword());
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("select * from Quotes.%ss WHERE ID=%d", type, i));
			rs.next();
			String message = String.format("%s %d: %s [%s]", type, rs.getInt(1), rs.getString(2), rs.getDate(4).toString());
			con.close();
			return message;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static String getRandom(String type)
	{
		try
		{
			String server = String.format("jdbc:mysql://%s/", Main.config.getSQLServer());
			Connection con = DriverManager.getConnection(server, Main.config.getSQLUser(), Main.config.getSQLPassword());
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(String.format("select * from Quotes.%ss ORDER BY RAND() LIMIT 1", type));
			rs.next();
			String message = String.format("%s %d: %s [%s]", type, rs.getInt(1), rs.getString(2), rs.getDate(4).toString());
			con.close();
			return message;
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
}