package com.danielm59.bcukbot.jokes;

import com.danielm59.bcukbot.Main;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.RequestBuffer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JokeAPI
{
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	private static BufferedReader request(URL url) throws Exception
	{
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("Accept", "text/plain");
		conn.setRequestProperty("User-Agent", "https://gitlab.com/BCUK_Dev/BCUK_Bot");
		if (conn.getResponseCode() != 200)
		{
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " From:" + url.toString());
		}
		return new BufferedReader(new InputStreamReader((conn.getInputStream())));
	}

	public static String getJoke()
	{
		try
		{
			BufferedReader br = request(new URL("https://icanhazdadjoke.com/"));
			String output;
			if ((output = br.readLine()) != null)
			{
				return output;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		Main.getLogger().error("Failed to get Joke");
		return "!API ERROR!";
	}

	public static void DiscordJoke(IChannel c)
	{
		RequestBuffer.request(() -> c.sendMessage(getJoke()));
	}

	public static void TwitchJoke(String channel)
	{
		Main.twitchbot.sendMessage(channel, getJoke());
	}
}
