package com.danielm59.bcukbot.games.trivia.api;

import com.danielm59.bcukbot.Main;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class TriviaAPI
{
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	private static String token;

	private static BufferedReader request(URL url) throws Exception
	{
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		if (conn.getResponseCode() != 200)
		{
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " From:" + url.toString());
		}
		return new BufferedReader(new InputStreamReader((conn.getInputStream())));
	}

	public static void getSessionToken()
	{
		try
		{
			BufferedReader br = request(new URL("https://opentdb.com/api_token.php?command=request"));
			String output;
			if ((output = br.readLine()) != null)
			{
				SessionToken tokenRequest = gson.fromJson(output, SessionToken.class);
				if (tokenRequest.getResponseCode() == 0)
				{
					token = tokenRequest.getToken();
				} else
				{
					Main.getLogger().error(String.format("Failed to get Trivia API token, Error code %d", tokenRequest.getResponseCode()));
				}
				return;
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
		Main.getLogger().error("Failed to get Trivia API token, no response");
	}

	public static com.danielm59.bcukbot.games.trivia.api.Questions.Question getQuestion() throws Exception
	{
		BufferedReader br = request(new URL(String.format("https://opentdb.com/api.php?amount=1&token=%s", token)));
		String output;
		Questions.Question question = null;
		if ((output = br.readLine()) != null)
		{
			Questions questionsRequest = gson.fromJson(output, Questions.class);
			question = processQuestionResponce(questionsRequest);
		}
		if (question == null) Main.getLogger().error("Failed to get Trivia question, no response");
		return question;
	}

	private static Questions.Question processQuestionResponce(Questions questionsRequest) throws Exception
	{
		switch (questionsRequest.getResponseCode())
		{
			case 0:
				return questionsRequest.getQuestions().get(0);
			case 1:
				Main.getLogger().error("No questions avaialble");
				return null;
			case 2:
				Main.getLogger().error("Invalid question request");
				return null;
			case 3:
				Main.getLogger().error("API token not found");
				getSessionToken();
				return getQuestion();
			case 4:
				Main.getLogger().error("Token out of questions");
				resetSessionToken();
				return getQuestion();
			default:
				Main.getLogger().error("Unknown response code");
				return null;
		}
	}

	private static void resetSessionToken() throws Exception
	{
		BufferedReader br = request(new URL(String.format("https://opentdb.com/api_token.php?command=reset&token=%s", token)));
		String output;
		if ((output = br.readLine()) != null)
		{
			SessionToken tokenRequest = gson.fromJson(output, SessionToken.class);
			if (tokenRequest.getResponseCode() == 0)
			{
				token = tokenRequest.getToken();
			} else
			{
				Main.getLogger().error(String.format("Failed to reset Trivia API token, Error code %d", tokenRequest.getResponseCode()));
			}
			return;
		}
		Main.getLogger().error("Failed to reset Trivia API token, no response");
	}
}
