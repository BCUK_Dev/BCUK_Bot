package com.danielm59.bcukbot.games.slot;

import java.util.Random;

public class SlotGame
{
	private static final Random rng = new Random();
	private static final String[] emojis = {"\uD83C\uDF47", "\uD83C\uDF4A", "\uD83C\uDF52", "\uD83C\uDF53"};

	public static String[] getOutcome()
	{
		return new String[]{random(), random(), random()};
	}

	private static String random()
	{
		return emojis[rng.nextInt(emojis.length)];
	}
}
