package com.danielm59.bcukbot.configs;

import java.io.File;

public interface JsonData
{
	File getFile();
}
