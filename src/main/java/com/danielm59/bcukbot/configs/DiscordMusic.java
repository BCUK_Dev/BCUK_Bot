package com.danielm59.bcukbot.configs;

import com.danielm59.bcukbot.Main;
import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class DiscordMusic implements JsonData
{
	private final transient File file = new File("playlists.json");

	@SerializedName("music_room_name")
	private String room = "Music Room";

	@SerializedName("music_volume")
	private int volume = 20;

	@SerializedName("playlists")
	private HashMap<String, String> playlists = new LinkedHashMap<>();

	public String getRoom()
	{
		return room;
	}

	public void setRoom(String room)
	{
		this.room = room;
		Main.saveJson(this);
	}

	public int getVolume()
	{
		return volume;
	}

	public void setVolume(int volume)
	{
		this.volume = volume;
		Main.saveJson(this);
	}

	public HashMap<String, String> getPlaylists()
	{
		return playlists;
	}

	public void addPlaylist(String name, String playlist)
	{
		playlists.put(name, playlist);
		Main.saveJson(this);
	}

	@Override
	public File getFile()
	{
		return file;
	}
}
