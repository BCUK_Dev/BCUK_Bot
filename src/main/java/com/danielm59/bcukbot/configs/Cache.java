package com.danielm59.bcukbot.configs;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class Cache implements JsonData
{
	private final transient File file = new File("cache.json");

	@SerializedName("users")
	private HashMap<String, String> users = new LinkedHashMap<>();
	@SerializedName("games")
	private HashMap<String, String> games = new LinkedHashMap<>();

	public HashMap<String, String> getUsers()
	{
		return users;
	}

	public void setUsers(HashMap<String, String> users)
	{
		this.users = users;
	}

	public HashMap<String, String> getGames()
	{
		return games;
	}

	public void setGames(HashMap<String, String> games)
	{
		this.games = games;
	}

	@Override
	public File getFile()
	{
		return file;
	}
}
