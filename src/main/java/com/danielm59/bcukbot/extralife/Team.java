package com.danielm59.bcukbot.extralife;

import com.google.gson.annotations.SerializedName;

class Team
{
	@SerializedName("fundraisingGoal")
	private double fundraisingGoal;
	@SerializedName("isInviteOnly")
	private Boolean isInviteOnly;
	@SerializedName("captainDisplayName")
	private String captainDisplayName;
	@SerializedName("eventName")
	private String eventName;
	@SerializedName("avatarImageURL")
	private String avatarImageURL;
	@SerializedName("createdDateUTC")
	private String createdDateUTC;
	@SerializedName("eventID")
	private Integer eventID;
	@SerializedName("sumDonations")
	private double sumDonations;
	@SerializedName("teamID")
	private Integer teamID;
	@SerializedName("name")
	private String name;
	@SerializedName("numDonations")
	private Integer numDonations;

	double getSumDonations()
	{
		return sumDonations;
	}
}

