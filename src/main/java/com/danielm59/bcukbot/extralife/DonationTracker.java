package com.danielm59.bcukbot.extralife;

import com.danielm59.bcukbot.Main;

public class DonationTracker
{
	private static double total = 0.00;

	public static void checkForUpdates()
	{
		Team team = ExtraLifeAPI.getTeamData(42098);
		if (team != null)
		{
			double newTotal = team.getSumDonations();
			if (newTotal > total)
			{
				total = newTotal;
				String message = String.format("The Battle Cattle Extra Life Total has reached $%10.2f", total);
				Main.twitchbot.sendMessage("#danielm59", message);
				Main.twitchbot.sendMessage("#mr_stephen", message);
				Main.twitchbot.sendMessage("#vimeous", message);
			}
		}
	}

	public static void postTotal(String channel)
	{
		String message = String.format("The Battle Cattle Extra Life Total is $%10.2f", total);
		Main.twitchbot.sendMessage(channel, message);
	}
}
