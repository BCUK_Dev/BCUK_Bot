package com.danielm59.bcukbot.twitch.api;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.twitch.api.data.GameData;
import com.danielm59.bcukbot.twitch.api.data.UserData;

public class CacheGetters
{
	public static String getUsername(String userID) throws Exception
	{
		if (Main.cache.getUsers().containsKey(userID))
		{
			return Main.cache.getUsers().get(userID);
		} else
		{
			UserData data = TwitchGetters.getUserData(userID);
			String username = data.getData().getDisplayName();
			Main.cache.getUsers().put(userID, username);
			Main.saveJson(Main.cache);
			return username;
		}
	}

	public static String getGame(String gameID) throws Exception
	{
		String gamename = "";
		if (Main.cache.getGames().containsKey(gameID))
		{
			gamename = Main.cache.getGames().get(gameID);
		} else
		{
			GameData data = TwitchGetters.getGameData(gameID);
			gamename = data.getData().getName();
			Main.cache.getGames().put(gameID, gamename);
			Main.saveJson(Main.cache);
		}
		return gamename;
	}
}
