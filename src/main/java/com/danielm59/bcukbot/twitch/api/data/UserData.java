package com.danielm59.bcukbot.twitch.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserData
{

	@SerializedName("data")
	private List<Datum> data = null;

	public Datum getData()
	{
		return (data.size() > 0) ? data.get(0) : null;
	}

	public class Datum
	{

		@SerializedName("id")
		private String id;
		@SerializedName("login")
		private String login;
		@SerializedName("display_name")
		private String displayName;
		@SerializedName("type")
		private String type;
		@SerializedName("broadcaster_type")
		private String broadcasterType;
		@SerializedName("description")
		private String description;
		@SerializedName("profile_image_url")
		private String profileImageUrl;
		@SerializedName("offline_image_url")
		private String offlineImageUrl;
		@SerializedName("view_count")
		private Integer viewCount;

		public String getId()
		{
			return id;
		}

		public String getLogin()
		{
			return login;
		}

		public String getDisplayName()
		{
			return displayName;
		}

		public String getType()
		{
			return type;
		}

		public String getBroadcasterType()
		{
			return broadcasterType;
		}

		public String getDescription()
		{
			return description;
		}

		public String getProfileImageUrl()
		{
			return profileImageUrl;
		}

		public String getOfflineImageUrl()
		{
			return offlineImageUrl;
		}

		public Integer getViewCount()
		{
			return viewCount;
		}
	}
}