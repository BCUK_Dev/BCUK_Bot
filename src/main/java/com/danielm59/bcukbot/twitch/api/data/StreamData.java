package com.danielm59.bcukbot.twitch.api.data;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StreamData
{
	@SerializedName("data")

	private List<Stream> data = null;

	public List<Stream> getData()
	{
		return data;
	}

	public class Stream
	{
		@SerializedName("id")
		private String id;
		@SerializedName("user_id")
		private String userId;
		@SerializedName("game_id")
		private String gameId;
		@SerializedName("community_ids")
		private List<String> communityIds = null;
		@SerializedName("type")
		private String type;
		@SerializedName("title")
		private String title;
		@SerializedName("viewer_count")
		private Integer viewerCount;
		@SerializedName("started_at")
		private String startedAt;
		@SerializedName("language")
		private String language;
		@SerializedName("thumbnail_url")
		private String thumbnailUrl;

		public String getId()
		{
			return id;
		}

		public String getUserId()
		{
			return userId;
		}

		public String getGameId()
		{
			return gameId;
		}

		public List<String> getCommunityIds()
		{
			return communityIds;
		}

		public String getType()
		{
			return type;
		}

		public String getTitle()
		{
			return title;
		}

		public Integer getViewerCount()
		{
			return viewerCount;
		}

		public String getStartedAt()
		{
			return startedAt;
		}

		public String getLanguage()
		{
			return language;
		}

		public String getThumbnailUrl()
		{
			return thumbnailUrl;
		}
	}
}