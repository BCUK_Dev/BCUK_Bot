package com.danielm59.bcukbot.twitch.api;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.twitch.api.data.GameData;
import com.danielm59.bcukbot.twitch.api.data.StreamData;
import com.danielm59.bcukbot.twitch.api.data.UserData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class TwitchGetters
{
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	private static BufferedReader request(URL url) throws Exception
	{
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("Client-ID", Main.config.getTwitchClientID());
		if (conn.getResponseCode() != 200)
		{
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode() + " From:" + url.toString());
		}
		return new BufferedReader(new InputStreamReader((conn.getInputStream())));
	}

	public static StreamData getStreamData(List<String> channels) throws Exception
	{
		String request = String.join("&user_login=", channels);
		URL url = new URL(String.format("https://api.twitch.tv/helix/streams?user_login=%s", request));
		BufferedReader br = request(url);

		String output;
		if ((output = br.readLine()) != null)
		{
			return gson.fromJson(output, StreamData.class);
		}
		return null;
	}

	public static GameData getGameData(String gameID) throws Exception
	{
		URL url = new URL(String.format("https://api.twitch.tv/helix/games?id=%s", gameID));
		BufferedReader br = request(url);

		String output;
		if ((output = br.readLine()) != null)
		{
			return gson.fromJson(output, GameData.class);
		}
		return null;
	}

	public static UserData getUserData(String userID) throws Exception
	{
		URL url = new URL(String.format("https://api.twitch.tv/helix/users?id=%s", userID));
		BufferedReader br = request(url);

		String output;
		if ((output = br.readLine()) != null)
		{
			return gson.fromJson(output, UserData.class);
		}
		return null;
	}
}