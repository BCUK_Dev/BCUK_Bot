package com.danielm59.bcukbot.twitch;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.discord.sfx.SFXHandler;

import java.io.File;

public class SFX
{
	private static long lastSFX = -1;

	public static void play(String channel, String message)
	{
		long time = System.currentTimeMillis();
		if ((time - lastSFX > Main.sfxFiles.getDelay() * 1000) &
				Main.sfxFiles.getSounds().containsKey(message.toLowerCase()))
		{
			SFXHandler.loadAndPlay(
					Main.config.getTwitchChats().get(channel.replace("#", "")),
					getFilePath(Main.sfxFiles.getSounds().get(message.toLowerCase()))
								  );
			lastSFX = time;
		}
	}

	private static String getFilePath(String sound)
	{
		return "." +
				File.separator +
				Main.sfxFiles.getFolder() +
				File.separator +
				sound;
	}

	public static String getSFX()
	{
		StringBuilder SFXlist = new StringBuilder();
		for (String sfx : Main.sfxFiles.getSounds().keySet())
		{
			SFXlist.append(sfx).append(", ");
		}
		SFXlist.setLength(Math.max(SFXlist.length() - 2, 0));//remove last comma
		return SFXlist.toString();
	}

}
