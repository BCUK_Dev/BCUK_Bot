package com.danielm59.bcukbot.twitch.irc;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.commands.twitch.TwitchCommands;
import com.danielm59.bcukbot.twitch.SFX;
import org.jibble.pircbot.PircBot;

public class IRCBot extends PircBot
{
	private static TwitchCommands commands = new TwitchCommands();

	private boolean verbose;

	public IRCBot()
	{
		this.setName(Main.config.getTwitchBotname());
		verbose = false;
		try
		{
			this.connect("irc.chat.twitch.tv", 6667, Main.config.getTwitchOauth());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public void join(String channel)
	{
		this.joinChannel("#" + channel);
	}

	@Override
	protected void onConnect()
	{
		Main.getLogger().info("Connected to Twitch chat server");

		for (String channel : Main.config.getTwitchChats().keySet())
		{
			this.join(channel);
		}

	}

	@Override
	protected void onDisconnect()
	{
		Main.getLogger().info("Disconnected");

		try
		{
			Main.getLogger().info("Attempting to reconnect");
			this.reconnect();

			for (String channel : Main.config.getTwitchChats().keySet())
			{
				this.join(channel);
			}
		} catch (Exception var4)
		{
			Main.getLogger().info("Reconnect failed, waiting 10 seconds before retrying");

			try
			{
				Thread.sleep(10000L);
			} catch (InterruptedException var3)
			{
				Thread.currentThread().interrupt();
			}
		}

	}

	@Override
	protected void onMessage(String server, String sender, String login, String hostname, String message)
	{
		String[] command = message.split(" ", 2);

		if (server != null && commands.containsKey(command[0].toLowerCase()))
		{
			String[] args = {};
			if (command.length > 1)
				args = command[1].split(" ");
			commands.get(command[0].toLowerCase()).getFunction().accept(server, sender, args);
		}
		if (server != null)
		{
			SFX.play(server, command[0]);
		}
	}

	@Override
	public void log(String line)
	{
		if (verbose)
		{
			Main.getLogger().info(line);
		}
	}
}