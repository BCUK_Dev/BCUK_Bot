package com.danielm59.bcukbot.twitch;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.configs.Config;

public class OnlineTracker
{
	public static void checkStatus()
	{
		try
		{
			for (Config.DiscordChannelInfo info : Main.config.getDiscordChannelInfo())
			{
				LiveStreams.processStreams(info);
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
