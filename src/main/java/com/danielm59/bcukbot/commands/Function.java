package com.danielm59.bcukbot.commands;

import java.util.Objects;

@FunctionalInterface
public interface Function<T, U, V>
{
	void accept(T t, U u, V v);

	default Function<T, U, V> andThen(Function<? super T, ? super U, ? super V> after)
	{
		Objects.requireNonNull(after);

		return (l, m, n) -> {
			accept(l, m, n);
			after.accept(l, m, n);
		};
	}
}
