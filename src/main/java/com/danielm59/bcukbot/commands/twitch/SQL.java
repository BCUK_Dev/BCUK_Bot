package com.danielm59.bcukbot.commands.twitch;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.Utils;
import com.danielm59.bcukbot.sql.SQLServer;

public class SQL
{
	private static final String SOURCE = "twitch";

	public static void processCommand(String type, String channel, String[] strings)
	{
		String msg = null;
		if (strings.length == 0)
		{
			msg = SQLServer.getRandom(type);
		} else if (strings[0].equalsIgnoreCase("add") && strings.length > 1)
		{
			String[] quoteArray = new String[strings.length - 1];
			System.arraycopy(strings, 1, quoteArray, 0, quoteArray.length);
			String quote = String.join(" ", quoteArray);
			msg = SQLServer.add(type, quote, SOURCE);
		} else if (Utils.getInt(strings[0]) > 0)
		{
			msg = SQLServer.get(type, Utils.getInt(strings[0]));
		}
		if (msg == null)
		{
			msg = "ERROR: Incorrect syntax";
		}
		Main.twitchbot.sendMessage(channel, msg);
	}
}
