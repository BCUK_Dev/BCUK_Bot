package com.danielm59.bcukbot.commands.twitch;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.commands.Function;
import com.danielm59.bcukbot.extralife.DonationTracker;
import com.danielm59.bcukbot.jokes.JokeAPI;
import com.danielm59.bcukbot.twitch.LiveStreams;
import com.danielm59.bcukbot.twitch.SFX;

import java.util.HashMap;

public class TwitchCommands extends HashMap<String, TwitchCommands.Command>
{
	public TwitchCommands()
	{
		this.put("!joke", new Command(((c, u, args) -> SQL.processCommand("Joke", c, args))));
		this.put("!dadjoke", new Command((c, u, args) -> JokeAPI.TwitchJoke(c)));
		this.put("!multi", new Command((c, u, args) -> LiveStreams.postMultiTwitch(c)));

		this.put("!blamevimeouscheck", new Command((c, u, args) -> counterCheck(c, "BlameVimeous")));
		this.put("#blamevimeous", new Command((c, u, args) -> counter(c, "BlameVimeous")));

		this.put("!sfx", new Command((c, u, args) -> Main.twitchbot.sendMessage(c, SFX.getSFX())));

		this.put("!extralifetotal", new Command((c, u, args) -> DonationTracker.postTotal(c)));
	}

	private static void counter(String c, String name)
	{
		Main.twitchbot.sendMessage(c, Main.counters.increment(name));
	}

	private static void counterCheck(String c, String name)
	{
		Main.twitchbot.sendMessage(c, Main.counters.get(name));
	}

	public class Command
	{
		Function<String, String, String[]> function;

		Command(Function<String, String, String[]> function)
		{
			this.function = function;
		}

		public Function<String, String, String[]> getFunction()
		{
			return function;
		}
	}
}
