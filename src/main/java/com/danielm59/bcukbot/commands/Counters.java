package com.danielm59.bcukbot.commands;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.configs.JsonData;

import java.io.File;
import java.util.HashMap;

public class Counters implements JsonData
{
	private final transient File file = new File("counters.json");
	private HashMap<String, Counter> counters = new HashMap<>();

	@Override
	public File getFile()
	{
		return file;
	}

	public String increment(String name)
	{
		if (counters.containsKey(name))
		{
			String message = counters.get(name).increment();
			Main.saveJson(this);
			return message;
		} else
		{
			Main.getLogger().error(String.format("Counter %s not found", name));
			return null;
		}
	}

	public String get(String name)
	{
		if (counters.containsKey(name))
		{
			String message = counters.get(name).get();
			Main.saveJson(this);
			return message;
		} else
		{
			Main.getLogger().error(String.format("Counter %s not found", name));
			return null;
		}
	}

	class Counter
	{
		String incrementMessage;
		String message;
		int count;

		String increment()
		{
			return String.format(incrementMessage + "     " + message, ++count);
		}

		String get()
		{
			return String.format(message, count);
		}
	}
}
