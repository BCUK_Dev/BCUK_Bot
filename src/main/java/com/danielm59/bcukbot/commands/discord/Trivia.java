package com.danielm59.bcukbot.commands.discord;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.discord.DiscordBot;
import com.danielm59.bcukbot.discord.Emoji;
import com.danielm59.bcukbot.games.trivia.api.Questions.Question;
import com.danielm59.bcukbot.games.trivia.api.TriviaAPI;
import org.apache.commons.text.StringEscapeUtils;
import sx.blah.discord.handle.impl.obj.ReactionEmoji;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.RequestFuture;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Trivia
{
	private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	private static HashMap<String, Color> difficultyColor = new LinkedHashMap<>();

	static
	{
		difficultyColor.put("easy", new Color(0, 255, 0));
		difficultyColor.put("medium", new Color(255, 255, 0));
		difficultyColor.put("hard", new Color(255, 0, 0));
	}

	public static void trivia(IChannel channel)
	{
		try
		{
			Question question = TriviaAPI.getQuestion();
			EmbedBuilder builder = new EmbedBuilder();
			builder.withTitle(parseText(question.getQuestion()));
			builder.withColor(difficultyColor.get(question.getDifficulty()));

			switch (question.getType())
			{
				case "multiple":
					multiQuestion(builder, question, channel);
					break;
				case "boolean":
					boolQuestion(builder, question, channel);
					break;
				default:
					throw new IllegalArgumentException(question.getType());
			}
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void multiQuestion(EmbedBuilder builder, Question question, IChannel channel)
	{
		List<String> answers = question.getIncorrectAnswers();
		answers.add(question.getCorrectAnswer());
		Collections.shuffle(answers);
		StringBuilder s = new StringBuilder();
		s.append(Emoji.A.toString()).append(parseText(answers.get(0))).append("\n");
		s.append(Emoji.B.toString()).append(parseText(answers.get(1))).append("\n");
		s.append(Emoji.C.toString()).append(parseText(answers.get(2))).append("\n");
		s.append(Emoji.D.toString()).append(parseText(answers.get(3)));
		builder.appendDescription(s.toString());
		Main.getLogger().info(question.getCorrectAnswer());
		RequestFuture<IMessage> request = RequestBuffer.request(() -> channel.sendMessage(builder.build()));
		scheduler.schedule(() -> multiReactions(request), 1L, TimeUnit.SECONDS);
		scheduler.schedule(() -> multiAnswer(request, question, answers), 60L, TimeUnit.SECONDS);
	}

	private static void boolQuestion(EmbedBuilder builder, Question question, IChannel channel)
	{
		StringBuilder s = new StringBuilder();
		s.append(Emoji.TRUE.toString()).append("True").append("\n");
		s.append(Emoji.FALSE.toString()).append("False");
		builder.appendDescription(s.toString());
		Main.getLogger().info(question.getCorrectAnswer());
		RequestFuture<IMessage> request = RequestBuffer.request(() -> channel.sendMessage(builder.build()));
		scheduler.schedule(() -> boolReactions(request), 1L, TimeUnit.SECONDS);
		scheduler.schedule(() -> boolAnswer(request, question), 60L, TimeUnit.SECONDS);
	}

	private static void multiReactions(RequestFuture<IMessage> request)
	{
		try
		{
			while (!request.isDone())
			{
				Thread.sleep(100);
			}
			IMessage message = request.get();
			RequestBuffer.request(() -> message.addReaction(Emoji.A)).get();
			RequestBuffer.request(() -> message.addReaction(Emoji.B)).get();
			RequestBuffer.request(() -> message.addReaction(Emoji.C)).get();
			RequestBuffer.request(() -> message.addReaction(Emoji.D)).get();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void boolReactions(RequestFuture<IMessage> request)
	{
		try
		{
			while (!request.isDone())
			{
				Thread.sleep(100);
			}
			IMessage message = request.get();
			RequestBuffer.request(() -> message.addReaction(Emoji.TRUE)).get();
			RequestBuffer.request(() -> message.addReaction(Emoji.FALSE)).get();
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void multiAnswer(RequestFuture<IMessage> request, Question question, List<String> answers)
	{
		try
		{
			while (!request.isDone())
			{
				Thread.sleep(100);
			}
			IMessage message = request.get();
			if (message.getReactionByEmoji(Emoji.A) == null)
			{
				message = DiscordBot.getMessage(message.getLongID()); //reload the message if reactions are missing
			}
			IChannel channel = message.getChannel();
			int correctId = answers.indexOf(question.getCorrectAnswer());
			List<Integer> wrongId = new ArrayList<>(Arrays.asList(0, 1, 2, 3));
			wrongId.remove(correctId);
			ReactionEmoji[] reactions = new ReactionEmoji[]{Emoji.A, Emoji.B, Emoji.C, Emoji.D};
			List<IUser> correct = new ArrayList<>();
			if (message.getReactionByEmoji(reactions[correctId]) != null)
			{
				correct.addAll(message.getReactionByEmoji(reactions[correctId]).getUsers());
			}
			for (int id : wrongId)
			{
				if (message.getReactionByEmoji(reactions[id]) != null)
				{
					correct.removeAll(message.getReactionByEmoji(reactions[id]).getUsers());
				}
			}
			Main.getLogger().info(correct.toString());

			message.delete();
			correctAnswerPost(channel, question, reactions[correctId], answers.get(correctId), correct);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void boolAnswer(RequestFuture<IMessage> request, Question question)
	{
		try
		{
			while (!request.isDone())
			{
				Thread.sleep(100);
			}
			IMessage message = request.get();
			if (message.getReactionByEmoji(Emoji.TRUE) == null)
			{
				message = DiscordBot.getMessage(message.getLongID()); //reload the message if reactions are missing
			}
			IChannel channel = message.getChannel();
			List<IUser> correct = new ArrayList<>();
			ReactionEmoji correctEmoji;
			if (question.getCorrectAnswer().equals("True"))
			{
				if (message.getReactionByEmoji(correctEmoji = Emoji.TRUE) != null)
				{
					correct.addAll(message.getReactionByEmoji(Emoji.TRUE).getUsers());
				}
				if (message.getReactionByEmoji(Emoji.FALSE) != null)
				{
					correct.removeAll(message.getReactionByEmoji(Emoji.FALSE).getUsers());
				}
			} else
			{
				if (message.getReactionByEmoji(correctEmoji = Emoji.FALSE) != null)
				{
					correct.addAll(message.getReactionByEmoji(Emoji.FALSE).getUsers());
				}
				if (message.getReactionByEmoji(Emoji.TRUE) != null)
				{
					correct.removeAll(message.getReactionByEmoji(Emoji.TRUE).getUsers());
				}
			}
			Main.getLogger().info(correct.toString());
			message.delete();
			correctAnswerPost(channel, question, correctEmoji, question.getCorrectAnswer(), correct);
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void correctAnswerPost(IChannel channel, Question question, ReactionEmoji emoji, String answer, List<IUser> correct)
	{
		EmbedBuilder builder = new EmbedBuilder();
		builder.withColor(difficultyColor.get(question.getDifficulty()));
		builder.withTitle(parseText(question.getQuestion()));
		StringBuilder s = new StringBuilder();
		s.append(emoji.toString()).append(parseText(answer));
		builder.appendDescription(s.toString());
		if (correct.size() > 0)
		{
			s = new StringBuilder();
			s.append("Correct: ");
			s.append(correct.stream().map(IUser::getName).collect(Collectors.joining(", ")));
			builder.withFooterText(s.toString());
		}
		RequestBuffer.request(() -> channel.sendMessage(builder.build()));
	}


	private static String parseText(String text)
	{
		text = StringEscapeUtils.unescapeHtml4(text);
		text = text.replace("*", "\\*");
		text = text.replace("_", "\\_");
		text = text.replace("~", "\\~");
		text = text.replace("&amp;", "&");

		return text;
	}

}
