package com.danielm59.bcukbot.commands.discord;

import com.danielm59.bcukbot.games.slot.SlotGame;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.RequestBuffer;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Slot
{
	private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	private static final long delay = 1L;

	public static void startGame(IChannel channel)
	{
		String[] outcome = SlotGame.getOutcome();
		String[] display = new String[outcome.length];
		Arrays.fill(display, "\u2753");
		IMessage message = RequestBuffer.request(() -> channel.sendMessage(String.join(" ", display))).get();
		scheduler.schedule(() -> update(message, outcome, display, 0), delay, TimeUnit.SECONDS);
	}

	private static void update(IMessage message, String[] outcome, String[] display, int i)
	{
		display[i] = outcome[i];
		RequestBuffer.request(() -> message.edit(String.join(" ", display)));
		if (i++ < outcome.length)
		{
			int newIndex = i;
			scheduler.schedule(() -> update(message, outcome, display, newIndex), delay, TimeUnit.SECONDS);
		}
	}

}
