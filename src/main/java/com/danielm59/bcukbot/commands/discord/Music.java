package com.danielm59.bcukbot.commands.discord;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.discord.music.MusicHandler;
import org.apache.commons.lang3.math.NumberUtils;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.RequestBuffer;

import java.util.HashMap;
import java.util.Set;

public class Music
{

	public static void skip(IChannel c)
	{
		MusicHandler.skipTrack(c);
	}

	public static void play(IChannel c, String[] args)
	{
		if (args.length > 0)
			MusicHandler.loadAndPlay(c, args[0]);
	}

	public static void stop(IChannel c)
	{
		MusicHandler.stopAndClear(c, false);
	}

	public static void list(IChannel c, String[] args)
	{
		if (args.length > 0 && NumberUtils.isCreatable(args[0]))
		{
			MusicHandler.listTracks(c, NumberUtils.createInteger(args[0]));
		} else
		{
			MusicHandler.listTracks(c, 1);
		}
	}

	public static void playing(IChannel c)
	{
		MusicHandler.playing(c);
	}

	public static void playlist(IChannel c, String[] args)
	{
		if (args.length > 0)
		{
			HashMap<String, String> playlists = Main.musicConfig.getPlaylists();
			if (playlists.containsKey(args[0].toLowerCase()))
			{
				MusicHandler.playPlaylist(c, playlists.get(args[0].toLowerCase()));
			} else
			{
				RequestBuffer.request(() -> c.sendMessage("Playlist not found"));
			}
		}
	}

	public static void playlists(IChannel c)
	{
		Set<String> playlists = Main.musicConfig.getPlaylists().keySet();
		if (playlists.size() > 0)
		{
			StringBuilder s = new StringBuilder();
			for (String playlist : playlists)
			{
				s.append(playlist);
				s.append("\n");
			}
			RequestBuffer.request(() -> c.sendMessage(s.toString()));
		} else
		{
			RequestBuffer.request(() -> c.sendMessage("No playlists founds"));
		}
	}

	public static void addPlaylist(IChannel c, String[] args)
	{
		if (args.length > 1)
		{
			Main.musicConfig.addPlaylist(args[0].toLowerCase(), args[1]);
			RequestBuffer.request(() -> c.sendMessage("Added playlist: " + args[0].toLowerCase()));
		} else
		{
			RequestBuffer.request(() -> c.sendMessage("Expected name and playlist link/ID"));
		}

	}

	public static void volume(IChannel c, String[] args)
	{
		if (args.length > 0)
		{
			MusicHandler.setVolume(c, args[0]);
		} else
		{
			MusicHandler.getVolume(c);
		}
	}

	public static void setRoom(IChannel c, String[] args)
	{
		if (args.length > 0)
		{
			String room = String.join(" ", args);
			Main.musicConfig.setRoom(room);
			RequestBuffer.request(() -> c.sendMessage("Set room to: " + room));
		} else
		{
			RequestBuffer.request(() -> c.sendMessage("Expected room name"));
		}
	}

	public static void pause(IChannel c)
	{
		MusicHandler.togglePause(c);
	}

	public static void join(IChannel c, IUser u)
	{
		IVoiceChannel channel = u.getVoiceStateForGuild(c.getGuild()).getChannel();

		if (channel == null)
		{
			RequestBuffer.request(() -> c.sendMessage("You must join a channel first"));
			return;
		}

		RequestBuffer.request(channel::join);
	}

	public static void leave(IChannel c)
	{
		MusicHandler.stopAndClear(c, true);
	}
}
