package com.danielm59.bcukbot.commands.discord;

import com.danielm59.bcukbot.Main;
import com.danielm59.bcukbot.commands.Function;
import com.danielm59.bcukbot.discord.DiscordEvents;
import com.danielm59.bcukbot.discord.pointSystem.PointSystem;
import com.danielm59.bcukbot.jokes.JokeAPI;
import org.apache.commons.lang3.SystemUtils;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.PermissionUtils;
import sx.blah.discord.util.RequestBuffer;

import java.io.IOException;
import java.util.HashMap;

public class DiscordCommands extends HashMap<String, DiscordCommands.Command>
{
	public DiscordCommands()
	{
		final Permissions GENERAL = Permissions.SEND_MESSAGES;
		final Permissions ADMIN = Permissions.MANAGE_MESSAGES;

		String shoes = ":high_heel: :sandal: :boot: :mans_shoe: :athletic_shoe:";

		//General
		this.put("!commands", new Command((c, u, args) -> DiscordCommands.commands(c, u), GENERAL));
		this.put("#shoesforwillow", new Command((c, u, args) -> RequestBuffer.request(() -> c.sendMessage(shoes)), GENERAL));
		this.put("!reload", new Command((c, u, args) -> Main.reloadConfig(c), ADMIN));
		this.put("!roles", new Command((c, u, args) -> DiscordCommands.roles(c, u), GENERAL));
		this.put("!update", new Command((c, u, args) -> update(), ADMIN));

		//SQL
		this.put("!quote", new Command((c, u, args) -> SQL.processCommand("Quote", c, args), GENERAL));
		this.put("!clip", new Command((c, u, args) -> SQL.processCommand("Clip", c, args), GENERAL));
		this.put("!joke", new Command((c, u, args) -> SQL.processCommand("Joke", c, args), GENERAL));

		//Levels
		this.put("!points", new Command((c, u, args) -> PointSystem.points(c, u), ADMIN));//Temp admin for testing

		//music
		this.put("!play", new Command((c, u, args) -> Music.play(c, args), GENERAL));
		this.put("!skip", new Command((c, u, args) -> Music.skip(c), GENERAL));
		this.put("!stop", new Command((c, u, args) -> Music.stop(c), GENERAL));
		this.put("!list", new Command((c, u, args) -> Music.list(c, args), GENERAL));
		this.put("!playing", new Command((c, u, args) -> Music.playing(c), GENERAL));
		this.put("!playlist", new Command((c, u, args) -> Music.playlist(c, args), GENERAL));
		this.put("!playlists", new Command((c, u, args) -> Music.playlists(c), GENERAL));
		this.put("!addplaylist", new Command((c, u, args) -> Music.addPlaylist(c, args), ADMIN));
		this.put("!setroom", new Command((c, u, args) -> Music.setRoom(c, args), ADMIN));
		this.put("!volume", new Command((c, u, args) -> Music.volume(c, args), GENERAL));
		this.put("!pause", new Command((c, u, args) -> Music.pause(c), GENERAL));
		this.put("!join", new Command((c, u, args) -> Music.join(c, u), GENERAL));
		this.put("!leave", new Command((c, u, args) -> Music.leave(c), GENERAL));

		//games
		this.put("!trivia", new Command((c, u, args) -> Trivia.trivia(c), GENERAL));
		this.put("!slot", new Command((c, u, args) -> Slot.startGame(c), GENERAL));

		this.put("!dadjoke", new Command((c, u, args) -> JokeAPI.DiscordJoke(c), GENERAL));

		//counters
		this.put("!blamevimeouscheck", new Command((c, u, args) -> counterCheck(c, "BlameVimeous"), GENERAL));
		this.put("#blamevimeous", new Command((c, u, args) -> counter(c, "BlameVimeous"), GENERAL));
	}

	private static void commands(IChannel channel, IUser user)
	{
		StringBuilder s = new StringBuilder();
		s.append("```\nAvailable commands:\n");
		DiscordCommands commands = DiscordEvents.commands;
		for (String key : commands.keySet())
		{
			Command command = commands.get(key);
			if (PermissionUtils.hasPermissions(channel, user, command.getPermission()))
			{
				s.append(String.format("%s\n", key));
			}
		}
		s.append("```");
		RequestBuffer.request(() -> channel.sendMessage(s.toString()));
	}

	private static void roles(IChannel channel, IUser user)
	{
		StringBuilder s = new StringBuilder();
		if (!channel.isPrivate())
		{
			s.append("```\nRoles:\n");

			for (IRole role : channel.getGuild().getRoles())
			{
				s.append(role.getName());
				s.append(":");
				s.append(role.getLongID());
				s.append("\n");
			}

			s.append("```");
		} else
		{
			s.append("This command only works in a server");
		}
		user.getOrCreatePMChannel().sendMessage(s.toString());
	}

	private static void update()
	{
		try
		{
			if (SystemUtils.IS_OS_WINDOWS)
			{
				Runtime.getRuntime().exec(new String[]{"cmd.exe", "/c", "start java -jar Updater.jar"});
			} else if (SystemUtils.IS_OS_LINUX)
			{
				Runtime.getRuntime().exec(new String[]{"xfce4-terminal", "-e", "java -jar Updater.jar"});
			}
			System.exit(0);
		} catch (IOException e)
		{
			Main.getLogger().error(e.toString());
		}
	}

	private static void counter(IChannel c, String name)
	{
		RequestBuffer.request(() -> c.sendMessage(Main.counters.increment(name)));
	}

	private void counterCheck(IChannel c, String name)
	{
		RequestBuffer.request(() -> c.sendMessage(Main.counters.get(name)));
	}

	public class Command
	{
		Function<IChannel, IUser, String[]> function;
		Permissions permission;

		Command(Function<IChannel, IUser, String[]> function, Permissions permission)
		{
			this.function = function;
			this.permission = permission;
		}

		public Function<IChannel, IUser, String[]> getFunction()
		{
			return function;
		}

		public Permissions getPermission()
		{
			return permission;
		}
	}

}
