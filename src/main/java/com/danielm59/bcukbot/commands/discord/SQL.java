package com.danielm59.bcukbot.commands.discord;

import com.danielm59.bcukbot.Utils;
import com.danielm59.bcukbot.sql.SQLServer;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.RequestBuffer;

class SQL
{
	private static final String SOURCE = "discord";

	static void processCommand(String type, IChannel channel, String[] args)
	{
		String msg = null;
		if (args.length == 0)
		{
			msg = SQLServer.getRandom(type);
		} else if (args[0].equalsIgnoreCase("add") && args.length > 1)
		{
			String[] quoteArray = new String[args.length - 1];
			System.arraycopy(args, 1, quoteArray, 0, quoteArray.length);
			String quote = String.join(" ", quoteArray);
			msg = SQLServer.add(type, quote, SOURCE);
		} else if (Utils.getInt(args[0]) > 0)
		{
			msg = SQLServer.get(type, Utils.getInt(args[0]));
		}
		if (msg == null)
		{
			msg = "ERROR: Incorrect syntax";
		}
		String finalMsg = msg;
		RequestBuffer.request(() -> channel.sendMessage(finalMsg));
	}
}
