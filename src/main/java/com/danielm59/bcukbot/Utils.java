package com.danielm59.bcukbot;

public class Utils
{
	public static int getInt(String s)
	{
		try
		{
			return Integer.parseInt(s);
		} catch (NumberFormatException nfe)
		{
			return -1;
		}
	}
}
