package com.danielm59.bcukbot;

import com.danielm59.bcukbot.commands.Counters;
import com.danielm59.bcukbot.configs.Cache;
import com.danielm59.bcukbot.configs.Config;
import com.danielm59.bcukbot.configs.DiscordMusic;
import com.danielm59.bcukbot.configs.JsonData;
import com.danielm59.bcukbot.configs.SFXfiles;
import com.danielm59.bcukbot.discord.DiscordBot;
import com.danielm59.bcukbot.discord.pointSystem.PointSystem;
import com.danielm59.bcukbot.games.trivia.api.TriviaAPI;
import com.danielm59.bcukbot.twitch.OnlineTracker;
import com.danielm59.bcukbot.twitch.irc.IRCBot;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.RequestBuffer;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main
{
	private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();
	private static final Logger logger = LoggerFactory.getLogger("BCUK_BOT");
	public static Config config = new Config();
	public static Cache cache = new Cache();
	public static SFXfiles sfxFiles = new SFXfiles();
	public static DiscordMusic musicConfig = new DiscordMusic();
	public static IRCBot twitchbot;
	public static Counters counters = new Counters();

	public static void main(String[] args)
	{
		TriviaAPI.getSessionToken(); //Setup trivia API
		config = loadJson(config);
		cache = loadJson(cache);
		sfxFiles = loadJson(sfxFiles);
		musicConfig = loadJson(musicConfig);
		counters = loadJson(counters);
		DiscordBot.init();
		PointSystem.init();
		twitchbot = new IRCBot();
		scheduler.scheduleAtFixedRate(OnlineTracker::checkStatus, 60, 15, TimeUnit.SECONDS);
		//scheduler.scheduleAtFixedRate(DonationTracker::checkForUpdates, 60, 60, TimeUnit.SECONDS);
	}

	public static Logger getLogger()
	{
		return logger;
	}

	public static void reloadConfig(IChannel channel)
	{
		config = loadJson(config);
		sfxFiles = loadJson(sfxFiles);
		RequestBuffer.request(() -> channel.sendMessage("Config reloaded"));
	}

	public static <T extends JsonData> T loadJson(T data)
	{
		File file = data.getFile();
		if (file.exists() && !file.isDirectory())
		{
			try (FileReader reader = new FileReader(file))
			{
				JsonReader jsonReader = new JsonReader(reader);
				data = gson.fromJson(jsonReader, data.getClass());
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return saveJson(data); //Save to add anything new in the config
	}

	public static <T extends JsonData> T saveJson(T data)
	{
		File file = data.getFile();
		try (FileWriter writer = new FileWriter(file))
		{
			gson.toJson(data, writer);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return data;
	}

}
